package stp.notearkiv.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Data class holding information about Part objects.
 *
 * @author Team STP
 */

//Creating a entity, and tablename.
@Entity
@Table(name = "Part")
public class Part implements Serializable {
    
    @Id @GeneratedValue 
    @NotNull private int part_id;

    @NotNull private String name;
 
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "myparts")
    private transient List<User> user_parts;
    
    @ManyToMany(fetch=FetchType.LAZY, mappedBy = "parts")
    private transient List<Score> score_parts;

    /**
     * creates an empty Part object.
     */
    public Part() {
    }

    /**
     * creates a Part object with all arguments.
     *
     * @param name
     */
    public Part(String name) {
        this.name = name;
    }
     
    // The following is a standardset of setter and getter methods
    
    public int getPart_id() {
        return part_id;
    }

    public void setPart_id(int part_id) {
        this.part_id = part_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

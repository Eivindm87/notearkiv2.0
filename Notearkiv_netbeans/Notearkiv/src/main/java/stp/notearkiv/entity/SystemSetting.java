package stp.notearkiv.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Data class holding various system settings.
 *
 * @author Team STP
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "Systemsetting")

public class SystemSetting implements Serializable {

    @Id
    @NotNull
    private final String settings_id = "system_settings";
    private String serverurl, servicesurl, version_number, email_address, email_password, email_smtp, email_port, admin_name, admin_email, contact_email;
    
    @Column(columnDefinition="clob")
    private String about_info;

    public SystemSetting() {
    }

    /**
     *
     * @param serverurl
     * @param servicesurl
     * @param version_number
     * @param email_address
     * @param email_password
     * @param email_smtp
     * @param email_port
     * @param admin_name
     * @param admin_email
     * @param contact_email
     * @param about_info
     */
    public SystemSetting(
            String serverurl,
            String servicesurl,
            String version_number,
            String email_address,
            String email_password,
            String email_smtp,
            String email_port,
            String admin_name,
            String admin_email,
            String contact_email,
            String about_info) {
        this.serverurl = serverurl;
        this.servicesurl = servicesurl;
        this.version_number = version_number;
        this.email_address = email_address;
        this.email_password = email_password;
        this.email_smtp = email_smtp;
        this.email_port = email_port;
        this.admin_name = admin_name;
        this.admin_email = admin_email;
        this.contact_email = contact_email;
        this.about_info = about_info;
    }

    public String getSettings_id() {
        return settings_id;
    }

    public String getServerurl() {
        return serverurl;
    }

    public String getServicesurl() {
        return servicesurl;
    }

    public String getVersion_number() {
        return version_number;
    }

    public String getEmail_address() {
        return email_address;
    }

    public String getEmail_password() {
        return email_password;
    }

    public String getEmail_smtp() {
        return email_smtp;
    }

    public String getEmail_port() {
        return email_port;
    }

    public String getAdmin_name() {
        return admin_name;
    }

    public String getAdmin_email() {
        return admin_email;
    }

    public void setServerurl(String serverurl) {
        this.serverurl = serverurl;
    }

    public void setServicesurl(String servicesurl) {
        this.servicesurl = servicesurl;
    }

    public void setVersion_number(String version_number) {
        this.version_number = version_number;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public void setEmail_password(String email_password) {
        this.email_password = email_password;
    }

    public void setEmail_smtp(String email_smtp) {
        this.email_smtp = email_smtp;
    }

    public void setEmail_port(String email_port) {
        this.email_port = email_port;
    }

    public void setAdmin_name(String admin_name) {
        this.admin_name = admin_name;
    }

    public void setAdmin_email(String admin_email) {
        this.admin_email = admin_email;
    }

    public String getContact_email() {
        return contact_email;
    }

    public void setContact_email(String contact_email) {
        this.contact_email = contact_email;
    }

    public String getAbout_info() {
        return about_info;
    }

    public void setAbout_info(String about_info) {
        this.about_info = about_info;
    }
}

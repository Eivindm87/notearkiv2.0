package stp.notearkiv.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Data class holding information about score objects.
 *
 * @author Team STP
 */
@Entity
@Table(name = "SCORE")
public class Score implements Serializable {
    
    @Id @GeneratedValue
    @NotNull private int score_id;
    @NotNull private int archive_number;
    @NotNull private String title;
    private String label, grade, comment, pdfpath, mp3path;
 
    private Time duration;
    
    @NotNull @ManyToMany(fetch=FetchType.EAGER) @JoinTable(
      name="SCORE_COMPOSER",
      joinColumns=@JoinColumn(name="SCORE_MUSICIAN_ID", referencedColumnName="SCORE_ID"),
      inverseJoinColumns=@JoinColumn(name="MUSICIAN_SCORE_ID", referencedColumnName="MUSICIAN_ID"))
      private List<Musician> score_composers;
    
    @ManyToMany(fetch=FetchType.EAGER) @JoinTable(
      name="SCORE_ARTIST",
      joinColumns=@JoinColumn(name="SCORE_MUSICIAN_ID", referencedColumnName="SCORE_ID"),
      inverseJoinColumns=@JoinColumn(name="MUSICIAN_SCORE_ID", referencedColumnName="MUSICIAN_ID"))
      private List<Musician> score_artists;
    
    @ManyToMany(fetch=FetchType.EAGER) @JoinTable(
      name="SCORE_ARRANGER",
      joinColumns=@JoinColumn(name="SCORE_MUSICIAN_ID", referencedColumnName="SCORE_ID"),
      inverseJoinColumns=@JoinColumn(name="MUSICIAN_SCORE_ID", referencedColumnName="MUSICIAN_ID"))
      private List<Musician> score_arrangers;
    
    @ManyToMany(fetch=FetchType.EAGER) @JoinTable(
      name="SCORE_LYRICIST",
      joinColumns=@JoinColumn(name="SCORE_MUSICIAN_ID", referencedColumnName="SCORE_ID"),
      inverseJoinColumns=@JoinColumn(name="MUSICIAN_SCORE_ID", referencedColumnName="MUSICIAN_ID"))
      private List<Musician> score_lyricists;
    
    @ManyToMany(fetch=FetchType.EAGER) @JoinTable(
      name="SCORE_PART",
      joinColumns=@JoinColumn(name="SCORE_PART_ID", referencedColumnName="SCORE_ID"),
      inverseJoinColumns=@JoinColumn(name="PART_SCORE_ID", referencedColumnName="PART_ID"))
      private List<Part> parts;

    /**
     * Creates an empty score object.
     */
    public Score() {
    }
    
    /**
     * Creates a new score with necessary variables.
     * 
     * @param archive_number The score's physical archive number
     * @param title The score's title
     * @param label The label the score is released on
     * @param duration Duration of the label in format MM:SS
     * @param grade The graded value for the difficulty level, could be both letter and number
     * @param comment Optional comment 
     * @param composer 
     */
    public Score(int archive_number, String title, String label, Time duration, String grade, String comment, List<Musician> composer) {
        this.archive_number = archive_number;
        this.title = title;
        this.label = label;
        this.duration = duration;
        this.grade = grade;
        this.comment = comment;
        this.score_composers = composer;
    }
    
    public Score(int archive_number, String title, String label, Time duration, String grade, String comment, String pdfpath, String mp3path, List<Musician> composers, List<Musician> arrangers, List<Musician> lyricists, List<Musician> artists) {
        this.archive_number = archive_number;
        this.title = title;
        this.label = label;
        this.duration = duration;
        this.grade = grade;
        this.comment = comment;
        this.pdfpath = pdfpath;
        this.mp3path = mp3path;
        this.score_composers = composers;
        this.score_arrangers = arrangers;
        this.score_lyricists = lyricists;
        this.score_artists = artists;
    }
        public Score(int archive_number, String title, String label, Time duration, String grade, String comment, String pdfpath, String mp3path, List<Musician> composers, List<Musician> arrangers, List<Musician> lyricists, List<Musician> artists, List<Part>parts) {
        this.archive_number = archive_number;
        this.title = title;
        this.label = label;
        this.duration = duration;
        this.grade = grade;
        this.comment = comment;
        this.pdfpath = pdfpath;
        this.mp3path = mp3path;
        this.score_composers = composers;
        this.score_arrangers = arrangers;
        this.score_lyricists = lyricists;
        this.score_artists = artists;
        this.parts = parts;
    }

    // Following is a standard set of Getters and Setters
    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public int getScore_id() {
        return score_id;
    }

    public int getArchive_number() {
        return archive_number;
    }

    public void setArchive_number(int archive_number) {
        this.archive_number = archive_number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Time getDuration() {
        return duration;
    }
     public String getDurationString() {
         return duration.toString();
     }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    public String getPdfpath() {
        return pdfpath;
    }

    public void setPdfpath(String pdfpath) {
        this.pdfpath = pdfpath;
    }

    public List<Musician> getScore_composers() {
        return score_composers;
    }

    public List<Musician> getScore_arrangers() {
        return score_arrangers;
    }

    public List<Musician> getScore_lyricists() {
        return score_lyricists;
    }

    public List<Musician> getScore_artists() {
        return score_artists;
    }
    
    public String getMp3path() {
        return mp3path;
    }

    public void setMp3path(String mp3path) {
        this.mp3path = mp3path;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setScore_composers(List<Musician> score_composers) {
        this.score_composers = score_composers;
    }

    public void setScore_artists(List<Musician> score_artists) {
        this.score_artists = score_artists;
    }

    public void setScore_arrangers(List<Musician> score_arrangers) {
        this.score_arrangers = score_arrangers;
    }

    public void setScore_lyricists(List<Musician> score_lyricists) {
        this.score_lyricists = score_lyricists;
    }

    public void setScore_id(int score_id) {
        this.score_id = score_id;
    }
    
}
package stp.notearkiv.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Data class holding information about Playlist objects.
 *
 * @author Team STP
 */

//@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "Playlist")

public class Playlist implements Serializable {
    
    @Id @GeneratedValue
    @NotNull private int playlist_id;
    @NotNull private String title;
    private String venue, comment;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date_playlist;
    
    private Time time_playlist;
    
    @ManyToMany(fetch=FetchType.EAGER, mappedBy = "playlists")
    private List<User> users;
    

    /**
     * Creates an empty Playlist object.
     */
    public Playlist() {
    }

    /**
     * Creates a Playlist object.
     *
     * @param title The title of the playlist
     * @param venue The venue the concert for the playlist is held
     * @param comment Optional comment
     * @param date_playlist The date for the concert for the playlist
     * @param time_playlist The time of the concert for the playlist
     */
    public Playlist(String title, String venue, String comment, Date date_playlist, Time time_playlist) {
        this.title = title;
        this.venue = venue;
        this.comment = comment;
        this.date_playlist = date_playlist;
        this.time_playlist = time_playlist;
    }

    // Following is a standard set of Getters and Setters

    public int getPlaylist_id() {
        return playlist_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate_playlist() {
        return date_playlist;
    }

    public void setDate_playlist(Date date_playlist) {
        this.date_playlist = date_playlist;
    }

    public Time getTime_playlist() {
        return time_playlist;
    }

    public void setTime_playlist(Time time_playlist) {
        this.time_playlist = time_playlist;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setPlaylist_id(int playlist_id) {
        this.playlist_id = playlist_id;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
    
}
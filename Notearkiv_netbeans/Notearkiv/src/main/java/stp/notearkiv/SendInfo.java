package stp.notearkiv;

import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import stp.notearkiv.resource.SettingsResource;

/**
 *
 * @author Team STP
 */
@Stateless
public class SendInfo {
    
    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    
    @Inject
    SettingsResource setres;

    public void sendUserInfo(String user_name, String first_name, String last_name, String password) {

        //Email properties
        Properties props = new Properties();
        props.put("mail.smtp.host", setres.getEmailSmtp());
        props.put("mail.smtp.socketFactory.port", setres.getEmailPort());
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", setres.getEmailPort());

        //Creates and login session for Notearkivet Email account
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(setres.getEmailAddress(), setres.getEmailPassword());
            }
        });
        try {
            //Creating the email invitation
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(setres.getEmailAddress()));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(user_name));
            message.setSubject("Informasjon om din bruker");
            message.setText("Hei " + first_name + " " + last_name + "." + System.lineSeparator()+ 
                    "Du har blitt lagt til som bruker på webapplikasjonen Notearkivet. " 
                    +System.lineSeparator() + "Dette er ditt forhåndsgenererte passord: "
                    + password + ". For å lage et personlig passord, logg inn på Notearkivet: "+ setres.getServerUrl() + " og gå til 'Min side'. "
                    + System.lineSeparator() + "Ved eventuelle spørsmål, send en epost til: " + setres.getContactEmail());
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    public void sendNewPassord(String user_name, String first_name, String last_name, String password) {

        //Email properties
        Properties props = new Properties();
        props.put("mail.smtp.host", setres.getEmailSmtp());
        props.put("mail.smtp.socketFactory.port", setres.getEmailPort());
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", setres.getEmailPort());

        //Creates and login session for Notearkivet Email account
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(setres.getEmailAddress(), setres.getEmailPassword());
            }
        });
        try {
            //Creating the email invitation
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(setres.getEmailAddress()));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(user_name));
            message.setSubject("Informasjon om din bruker");
            message.setText("Hei " + first_name + " " + last_name + "." + System.lineSeparator()+ 
                    "Du har blitt tildelt et nytt midlertidig passord." 
                    +System.lineSeparator() + "Dette er ditt nye forhåndsgenererte passord: "
                    + password + ". For å lage et personlige passord, logg inn på Notearkivet: "+ setres.getServerUrl() + " og gå til 'Min side'. "
                    + System.lineSeparator() + "Ved eventuelle spørsmål, send en epost til: " + setres.getContactEmail());
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}

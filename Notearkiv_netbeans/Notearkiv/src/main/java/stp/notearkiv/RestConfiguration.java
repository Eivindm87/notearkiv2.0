package stp.notearkiv;

import javax.annotation.Resource;
import javax.sql.DataSource;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Class for configurations and settings to the Notearkivet application,
 * and setting the root application path.
 * 
 * @author Team STP
 */

@ApplicationPath("services") //this is the root path after application name.
public class RestConfiguration extends Application {
    
    // Database resource connection
    @Resource(mappedName = "jdbc/notearkiv")
    private DataSource dataSource;
}
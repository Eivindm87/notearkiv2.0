package stp.notearkiv.resource;

import com.google.gson.Gson;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import stp.notearkiv.PasswordEncryptionService;
import stp.notearkiv.SendInfo;
import stp.notearkiv.entity.Instrument;
import stp.notearkiv.entity.Musician;
import stp.notearkiv.entity.Part;
import stp.notearkiv.entity.Role;
import stp.notearkiv.entity.Score;
import stp.notearkiv.entity.User;

/**
 *
 * @author Team STP
 */
@Stateless
@Path("/import")
public class ImportResource {

    @EJB
    private SendInfo send;

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;

    @Inject
    SessionResource sr;

    /**
     * Imports users from client side CSV upload
     *
     * @param cookie
     * @param importList
     * @return a String version of a Json Array containing imported and/or
     * rejected users
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    @Path("/users")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public String importUser(@CookieParam("access_token") Cookie cookie, User[] importList) throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (!(sr.isAdmin(cookie)) && !(sr.isArkivansvarlig(cookie))) {
            String s = new Gson().toJson("nei");
            return s;
        }
        List<List<User>> output = new ArrayList<>();

        List<User> added = new ArrayList<>();
        List<User> declined = new ArrayList<>();

        output.add(added);
        output.add(declined);

        for (User u : importList) {

            boolean valid = false;
            int valid_count = 0;

            String first_name = "";
            String last_name = "";
            String street_address = "";
            String city = "";
            String email = "";
            String date_member = "";
            String user_name = "";

            int postal_code = 0;
            int phone = 0;

            String password = new PasswordEncryptionService().getTempPassword();

            List<Instrument> instruments = new ArrayList<>();
            List<Role> roles = new ArrayList<>();

            instruments = em.createQuery("select i from Instrument i where i.name like 'utøver'", Instrument.class).getResultList(); //empty list, used for syntax
            roles = em.createQuery("select r from Role r where r.name like 'utøver'", Role.class).getResultList(); //

            try {
                first_name = u.getFirst_name();
                if (first_name.length() > 0) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no firstname");
            }
            try {
                last_name = u.getLast_name();
                if (last_name.length() > 0) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no last_name");
            }
            try {
                street_address = u.getStreet_address();
                if (street_address.length() > 0) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no street_address");
            }
            try {
                postal_code = u.getPostal_code();
                if (String.valueOf(postal_code).length() == 4) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no postal_code");
            }
            try {
                city = u.getCity();
                if (city.length() > 0) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no city");
            }
            try {
                email = u.getEmail();

                if (email.length() > 0) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no email");
            }
            try {
                phone = u.getPhone();

                if (String.valueOf(phone).length() == 8) {
                    valid = true;
                    valid_count++;
                } else {
                    System.out.println("ugyldig nummer, kan ikke ha space");
                }

            } catch (NullPointerException e) {
                System.out.println("no phone");
            }
            try {
                user_name = u.getUser_name();
                List check = em.createQuery("SELECT u FROM User u WHERE (u.user_name) = (:paramID)", User.class).setParameter("paramID", user_name).getResultList();
                if ((user_name.length() > 0) && (check.isEmpty())) {
                    valid = true;
                    valid_count++;
                }

            } catch (NullPointerException e) {
                System.out.println("no user_name");
            }

            if ((valid == true) && (valid_count == 8)) {
                User a = new User(first_name, last_name, street_address, postal_code, city, email, phone, date_member, instruments, user_name, password, roles);
                em.persist(a);

                User t = new User();
                t.setFirst_name(first_name);
                t.setLast_name(last_name);
                t.setUser_name(user_name);
                added.add(t);

                send.sendUserInfo(user_name, first_name, last_name, password);  //sends an email notice to the added user
            } else {
                declined.add(u);
            }
        }

        String json = (String) new Gson().toJson(output);
        return json;
    }

    /**
     * Imports scores from client side CSV upload
     *
     * @param cookie
     * @param importList
     * @return a String version of a Json Array containing imported and/or
     * rejected scores
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    @Path("/scores")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public String importScores(@CookieParam("access_token") Cookie cookie, Score[] importList) throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (!(sr.isAdmin(cookie)) && !(sr.isArkivansvarlig(cookie))) {
            String s = new Gson().toJson("nei");
            return s;
        }
        List<List<Score>> output = new ArrayList<>();

        List<Score> added = new ArrayList<>();
        List<Score> declined = new ArrayList<>();

        output.add(added);
        output.add(declined);

        for (Score s : importList) {

            boolean exist = false;
            boolean error = false;

            while (exist == false && error == false) {

                int valid_count = 0;

                int archive_number = 0;
                String title = "";
                String label = "";
                String grade = "";
                String comment = "";
                String duration = ""; //Time
                String pdfpath = "";
                String mp3path = "";
                List<Musician> score_composers = new ArrayList<>();
                List<Musician> score_arrangers = new ArrayList<>();
                List<Musician> score_lyricists = new ArrayList<>();
                List<Musician> score_artists = new ArrayList<>();
                List<Part> parts = new ArrayList<>();

                try {
                    title = s.getTitle();
                    if (title.length() > 0) {
                        title = title.replaceAll("&amp;", "&");
                        valid_count++;

                        List tempcheck = em.createQuery("SELECT s FROM Score s WHERE (s.title) = (:paramID)", Score.class).setParameter("paramID", title).getResultList();
                        if (!tempcheck.isEmpty()) {
                            Score tempscore = (Score) tempcheck.get(0);
                            score_composers = s.getScore_composers();

                            if (!score_composers.isEmpty()) {
                                List<Musician> scorecomposerslinked = new ArrayList<>();
                                for (Musician m : score_composers) {
                                    String name = m.getName();
                                    if (name.length() > 0) {
                                        List<String> list = new ArrayList<>(Arrays.asList(name.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
                                        Collections.replaceAll(list, "&nbsp;", " ");
                                        for (String names : list) {
                                            List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                            if (!temp.isEmpty()) {
                                                scorecomposerslinked.add((Musician) temp.get(0));
                                            }
                                        }
                                    }

                                    score_composers = scorecomposerslinked;
                                    if (tempscore.getScore_composers().equals(score_composers)) {
                                        valid_count = -8;
                                        exist = true;
                                    }
                                }
                            }
                        }
                    }

                } catch (NullPointerException e) {
                    exist = true;
                    System.out.println("no title");
                }

                try {
                    archive_number = s.getArchive_number();
                    valid_count++;

                } catch (NullPointerException e) {
                    archive_number = 0;
                    valid_count++;
                    System.out.println("no archive number");
                }

                try {
                    String temp = s.getLabel();
                    //sjekk for &amp; og replace med &
                    label = temp.replaceAll("&amp;", "&");

                } catch (NullPointerException e) {
                    label = "";
                    System.out.println("no label");
                }
                try {
                    grade = s.getGrade();

                } catch (NullPointerException e) {
                    grade = "";
                    System.out.println("no grade");
                }
                try {
                    comment = s.getComment();
                    comment = comment.replaceAll("&amp;", "&");

                } catch (NullPointerException e) {
                    comment = "";
                    System.out.println("no comment");
                }
                try {
                    parts = s.getParts();
                    if (!parts.isEmpty()) {
                        List<Part> scorepart = new ArrayList<>();
                        String id = "";

                        for (int i = 0; i < parts.size(); i++) {
                            id += parts.get(i).getPart_id();
                            if (i < parts.size() - 1) {
                                id += ", ";
                            }

                        }
                        List<Part> temp = em.createQuery("select p from Part p where p.part_id in(" + id + ")", Part.class).getResultList();
                        if (!temp.isEmpty()) {
                            scorepart = temp;
                        }

                        parts = scorepart;
                    }

                } catch (NullPointerException e) {
                    List<Part> partlinked = new ArrayList<>();
                    parts = partlinked;
                    System.out.println("no parts");
                }

                try {
                    duration = s.getDurationString();
                    duration = duration.replaceAll("'+01:00'", "");

                } catch (NullPointerException e) {
                    duration = "00:00:00";
                    System.out.println("no duration, made temp");
                }
                try {
                    pdfpath = s.getPdfpath();

                } catch (NullPointerException e) {
                    pdfpath = "";
                    System.out.println("no pdfpath, la til space");
                }
                try {
                    mp3path = s.getMp3path();

                } catch (NullPointerException e) {
                    mp3path = "";
                    System.out.println("no mp3path, la til space");
                }
                if (valid_count >= 2) {
                    try {
                        if (valid_count >= 2) {
                            score_composers = s.getScore_composers();
                        }
                        if (!score_composers.isEmpty()) {
                            List<Musician> scorecomposerslinked = new ArrayList<>();
                            for (Musician m : score_composers) {
                                String name = m.getName();
                                if (name.length() > 0) {
                                    List<String> list = new ArrayList<>(Arrays.asList(name.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
                                    for (String names : list) {
                                        List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                        if (!temp.isEmpty()) {
                                            scorecomposerslinked.add((Musician) temp.get(0));
                                        } else {
                                            em.persist(new Musician(names));
                                            List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                            if (!temp1.isEmpty()) {
                                                scorecomposerslinked.add((Musician) temp1.get(0));
                                            }
                                        }
                                    }
                                } else {
                                    valid_count = -8;
                                }

                                score_composers = scorecomposerslinked;
                                valid_count++;
                            }
                        }

                    } catch (NullPointerException e) {
                        valid_count = -8;
                        System.out.println("no composer");
                    }
                    if (valid_count >= 3) {
                        try {
                            score_arrangers = s.getScore_arrangers();
                            if (!score_arrangers.isEmpty()) {
                                List<Musician> scorearrangerslinked = new ArrayList<>();
                                for (Musician m : score_arrangers) {
                                    String name = m.getName();
                                    if (name.length() > 0) {
                                        List<String> list = new ArrayList<>(Arrays.asList(name.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
                                        for (String names : list) {
                                            List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                            if (!temp.isEmpty()) {
                                                scorearrangerslinked.add((Musician) temp.get(0));
                                            } else {
                                                em.persist(new Musician(names));
                                                List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                                if (!temp1.isEmpty()) {
                                                    scorearrangerslinked.add((Musician) temp1.get(0));
                                                }
                                            }
                                        }
                                    }
                                }
                                score_arrangers = scorearrangerslinked;
                            }

                        } catch (NullPointerException e) {
                            System.out.println("no arrangers");
                        }
                        try {
                            score_lyricists = s.getScore_lyricists();
                            if (!score_lyricists.isEmpty()) {
                                List<Musician> scorelyricistslinked = new ArrayList<>();
                                for (Musician m : score_lyricists) {
                                    String name = m.getName();
                                    if (name.length() > 0) {
                                        List<String> list = new ArrayList<>(Arrays.asList(name.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
                                        for (String names : list) {
                                            List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                            if (!temp.isEmpty()) {
                                                scorelyricistslinked.add((Musician) temp.get(0));
                                            } else {
                                                em.persist(new Musician(names));
                                                List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                                if (!temp1.isEmpty()) {
                                                    scorelyricistslinked.add((Musician) temp1.get(0));
                                                }
                                            }
                                        }
                                    }
                                }
                                score_lyricists = scorelyricistslinked;
                            }

                        } catch (NullPointerException e) {
                            System.out.println("no arrangers");
                        }
                        try {
                            score_artists = s.getScore_artists();
                            if (!score_artists.isEmpty()) {
                                List<Musician> scoreartistslinked = new ArrayList<>();
                                for (Musician m : score_artists) {
                                    String name = m.getName();
                                    if (name.length() > 0) {
                                        List<String> list = new ArrayList<>(Arrays.asList(name.split("(\\s*,\\s*)|(\\s*/\\s*)|(\\s* - \\s*)|(\\s*&amp;\\s*)|(\\s*&\\s*)|(\\s* and \\s*)|(\\s* og \\s*)")));
                                        for (String names : list) {
                                            List temp = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                            if (!temp.isEmpty()) {
                                                scoreartistslinked.add((Musician) temp.get(0));
                                            } else {
                                                em.persist(new Musician(names));
                                                List temp1 = em.createQuery("SELECT m FROM Musician m WHERE (m.name) = (:paramID)", Musician.class).setParameter("paramID", names).getResultList();
                                                if (!temp1.isEmpty()) {
                                                    scoreartistslinked.add((Musician) temp1.get(0));
                                                }
                                            }
                                        }
                                    }
                                }
                                score_artists = scoreartistslinked;
                            }

                        } catch (NullPointerException e) {
                            System.out.println("no artists");
                        }
                    }
                }
                if (valid_count >= 3) {
                    Score sc = new Score(archive_number, title, label, Time.valueOf(duration), grade, comment, pdfpath, mp3path, score_composers, score_arrangers, score_lyricists, score_artists, parts);
                    em.persist(sc);
                    added.add(sc);
                    exist = true;
                } else {
                    declined.add(s);
                    error = true;
                    exist = true;
                }
                error = true;
                exist = true;
            }
        }

        Object[] a = output.toArray();

        String json = (String) new Gson().toJson(a);
        return json;
    }

}

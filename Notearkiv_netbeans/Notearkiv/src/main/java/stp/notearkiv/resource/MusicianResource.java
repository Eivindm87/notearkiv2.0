package stp.notearkiv.resource;

import java.security.Key;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import stp.notearkiv.entity.Musician;

/**
 *
 * @author Team STP
 */
@Stateless
@Path("/musicians")
public class MusicianResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    // often used string to select user in database
    private final String selectMusicians = "SELECT m from Musician m";
    private final Key key = Resource.KEY;

    /**
     * Return a list of all registered Musicians
     *
     * @return a list of all Musicians
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Musician> getAllMusicians() {
        return em.createQuery(selectMusicians, Musician.class).getResultList();
    }

    @Path("/find")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Musician> findMusicians(@DefaultValue("") @QueryParam("search") String search) {
        String MusiciansToGet = "";
        MusiciansToGet += "lower (m.name) like lower ('%" + search + "%')";
        List<Musician> foundMusician;
        foundMusician = em.createQuery(selectMusicians + " WHERE (" + MusiciansToGet + ")", Musician.class).getResultList();
        return foundMusician;
    }

}

package stp.notearkiv.resource;

import stp.notearkiv.entity.User;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;
import java.security.Key;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.QueryParam;
import stp.notearkiv.PasswordEncryptionService;
import stp.notearkiv.SendInfo;
import stp.notearkiv.entity.Instrument;
import stp.notearkiv.entity.Part;
import stp.notearkiv.entity.Role;

/**
 *
 * @author Team STP
 */
@Stateless
@Path("/users")
public class UserResource {

    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    @EJB
    private SendInfo send;

    @Inject
    SessionResource sr;
    // often used string to select user in database
    private final String selectUsers = "SELECT u from User u";
    private final Key key = Resource.KEY;

    /**
     * Return a list of all registered users
     *
     * @param cookie
     * @return a list of all users
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getAllUsers(@CookieParam("access_token") Cookie cookie) {
        if(sr.sessionCheck(cookie).getStatus()==200){
            return em.createQuery(selectUsers, User.class).getResultList();
        }else{
            List<User> error =new ArrayList<>();
            return error;
        }
        
    }

    /**
     * Create a new user, or edit existing one
     *
     * @param first_name
     * @param last_name
     * @param street_address
     * @param postal_code
     * @param city
     * @param phone
     * @param date_member
     * @param instrumentID
     * @param partID
     * @param password1
     * @param password2
     * @param email
     * @param user_name
     * @param roleID
     * @return a list of the new or edited user
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> addUser(
            @FormParam("first_name") String first_name,
            @FormParam("last_name") String last_name,
            @FormParam("street_address") String street_address,
            @FormParam("postal_code") int postal_code,
            @FormParam("city") String city,
            @FormParam("email") String email,
            @FormParam("phone") int phone,
            @FormParam("date_member") String date_member,
            @FormParam("instrument") int instrumentID,
            @FormParam("part") int partID,
            @FormParam("user_name") String user_name,
            @FormParam("password1") String password1,
            @FormParam("password2") String password2,
            @FormParam("role") int roleID) throws NoSuchAlgorithmException, InvalidKeySpecException {

        // create list of instruments connected to the user
        List<Instrument> userInstruments = new ArrayList<>();
        List result = em.createQuery("SELECT i FROM Instrument i WHERE (i.instrument_id) = (:paramID)", Instrument.class).setParameter("paramID", instrumentID).getResultList();
        Instrument i = null;
        if (!result.isEmpty()) {
            i = (Instrument) result.get(0);
            userInstruments.add(i);
        }

        // create list of roles connected to the user
        List<Role> userRoles = new ArrayList<>();
        String name = "utøver";
        List result2 = em.createQuery("SELECT r FROM Role r WHERE (r.role_id) = (:paramID)", Role.class).setParameter("paramID", roleID).getResultList();
        List result3 = em.createQuery("SELECT r FROM Role r WHERE (r.name) = (:paramID)", Role.class).setParameter("paramID", name).getResultList();
        Role r = null;
        if (result2.isEmpty()) {
            r = (Role) result3.get(0);
            userRoles.add(r);
        }
        if (!result2.isEmpty()) {
            r = (Role) result2.get(0);
            userRoles.add(r);
        }
        if (date_member == null) {
            date_member = "";
        }

        // create the new user with all arguments
        User u = new User(
                first_name,
                last_name,
                street_address,
                postal_code,
                city,
                email,
                phone,
                date_member,
                userInstruments,
                user_name,
                password1,
                userRoles);

        // add parts connected to the users instrument
        List result1 = em.createQuery("SELECT p FROM Part p WHERE (p.part_id) = (:paramID)", Part.class).setParameter("paramID", partID).getResultList();
        if (!result1.isEmpty()) {
            u.setMyparts(result1);
        }

        em.persist(u);
        List<User> resultList = new ArrayList<>();
        resultList.add(em.find(User.class, u.getUser_id()));
        return resultList;
        
        //return em.createQuery(selectUsers, User.class).getResultList();
    }

    /**
     * Returns the logged in user
     *
     * @param cookie the cookie for the logged in user
     * @return the logged in user
     */
    @Path("/myuser")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getMyUser(@CookieParam("access_token") Cookie cookie) {

        List<User> temp = null;
        try {
            int id = Integer.valueOf(Jwts.parser().setSigningKey(key).parseClaimsJws(cookie.getValue()).getBody().getId());
            temp = em.createQuery("SELECT u FROM User u WHERE (u.user_id) = (:paramID)", User.class).setParameter("paramID", id).getResultList();

            //OK, we can trust this JWT
        } catch (SignatureException e) {
            //don't trust the JWT!
        }

        return temp;
    }

    /**
     *
     * @param id the userid of the wanted user
     * @return a user as json
     */
    @Path("/{USER_ID}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public User getUser(@PathParam("USER_ID") int id) {
        return em.find(User.class, id);
    }

    /**
     * Return a user based on user name
     *
     * @param user_name the user name of the wanted user
     * @return a user as json
     */
    @Path("/get/{USER_NAME}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public User getUserByUserName(@PathParam("USER_NAME") String user_name) {
        List result = em.createQuery("SELECT u FROM User u WHERE (u.user_name) = (:paramID)", User.class).setParameter("paramID", user_name).getResultList();
        User temp = null;
        if (!result.isEmpty()) {
            temp = (User) result.get(0);
        }

        return temp;
    }

    /**
     * @param cookie
     * @param id
     * @return
     */
    @Path("/deactivate/{USER_ID}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response deactivateUser(@CookieParam("access_token") Cookie cookie, @PathParam("USER_ID") int id) {

        if (sr.isAdmin(cookie) || sr.isArkivansvarlig(cookie)) {
            User u = em.find(User.class, id);
            u.setIsActive(false);
            em.persist(u);
            return Response.ok("deaktivert").build();

        } else {
            return Response.serverError().build();
        }
    }

    /**
     *
     * @param cookie
     * @param id
     * @return
     */
    @Path("/activate/{USER_ID}")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response activateUser(@CookieParam("access_token") Cookie cookie, @PathParam("USER_ID") int id) {

        if (sr.isAdmin(cookie) || sr.isArkivansvarlig(cookie)) {

            User u = em.find(User.class, id);
            u.setIsActive(true);
            em.persist(u);

            return Response.ok("aktivert").build();

        } else {
            return Response.serverError().build();
        }

    }

    /**
     *
     * @param user_name
     * @param email
     * @return
     */
    @Path("/istaken")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response isTaken(
            @QueryParam("user_name") String user_name,
            @QueryParam("email") String email) {

        String result = "false";
        List<User> temp1;
        List<User> temp2;
        if (user_name != null && !user_name.isEmpty()) {
            temp1 = em.createQuery(selectUsers + " WHERE (u.user_name) = (:paramID)", User.class).setParameter("paramID", user_name).getResultList();
            if (!temp1.isEmpty()) {
                result = "true";
            }
        }
        if (email != null && !email.isEmpty()) {
            temp2 = em.createQuery(selectUsers + " WHERE (u.email) = (:paramID)", User.class).setParameter("paramID", email).getResultList();
            if (!temp2.isEmpty()) {
                result = "true";
            }
        }

        return Response.ok(result).build();
    }

    /**
     * @param cookie
     * @param first_name
     * @param last_name
     * @param street_address
     * @param postal_code
     * @param city
     * @param email
     * @param phone
     * @param date_member
     * @param instrumentID
     * @param partID
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/altermyuser")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> alterMyUser(@CookieParam("access_token") Cookie cookie,
            @FormParam("first_name") String first_name,
            @FormParam("last_name") String last_name,
            @FormParam("street_address") String street_address,
            @FormParam("postal_code") int postal_code,
            @FormParam("city") String city,
            @FormParam("email") String email,
            @FormParam("phone") int phone,
            @FormParam("date_member") String date_member,
            @FormParam("instrument") int instrumentID,
            @FormParam("part") int partID) throws NoSuchAlgorithmException, InvalidKeySpecException {

        User testme = getMyUser(cookie).get(0);
        User me = em.find(User.class, testme.getUser_id());
        me.setFirst_name(first_name);
        me.setLast_name(last_name);
        me.setStreet_address(street_address);
        me.setPostal_code(postal_code);
        me.setCity(city);
        me.setEmail(email);
        me.setPhone(phone);
        me.setDate_member(date_member);

        // create list of instruments connected to the user
        List<Instrument> userInstruments = new ArrayList<>();
        List result = em.createQuery("SELECT i FROM Instrument i WHERE (i.instrument_id) = (:paramID)", Instrument.class).setParameter("paramID", instrumentID).getResultList();
        Instrument i = null;
        me.setInstruments(userInstruments);
        if (!result.isEmpty()) {
            i = (Instrument) result.get(0);
            userInstruments.add(i);
            me.setInstruments(userInstruments);
        }

        // add parts connected to the users instrument
        List result1 = em.createQuery("SELECT p FROM Part p WHERE (p.part_id) = (:paramID)", Part.class).setParameter("paramID", partID).getResultList();
        if (!result1.isEmpty()) {
            me.setMyparts(result1);
        } else if (result1.isEmpty()) {
            List<Part> p = null;
            me.setMyparts(p);
        }

        if (date_member == null) {
            date_member = "";
        }
        me.updateDate_edited();
        em.persist(me);
        return em.createQuery(selectUsers, User.class).getResultList();
    }

    /**
     *
     * @param cookie
     * @param password1
     * @param password2
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/altermypassword")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public void alterMyPassword(@CookieParam("access_token") Cookie cookie,
            @FormParam("password1") String password1,
            @FormParam("password2") String password2) throws NoSuchAlgorithmException, InvalidKeySpecException {

        User testme = getMyUser(cookie).get(0);
        User me = em.find(User.class, testme.getUser_id());

        if (!password1.isEmpty() || !password2.isEmpty()) {
            PasswordEncryptionService a = new PasswordEncryptionService();
            me.setSalt(a.generateSalt());
            me.setPassword(a.getEncryptedPassword(password1, me.getSalt()));
        }

        me.updateDate_edited();
        em.persist(me);

    }

    /**
     *
     * @param user_name
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/forgotpassword")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response forgotPassword(@FormParam("user_name") String user_name) throws NoSuchAlgorithmException, InvalidKeySpecException {

        User me = getUserByUserName(user_name);
        PasswordEncryptionService a = new PasswordEncryptionService();
        me.setSalt(a.generateSalt());
        String tempPassword = a.getTempPassword();
        me.setPassword(a.getEncryptedPassword(tempPassword, me.getSalt()));
        em.persist(me);
        send.sendNewPassord(me.getUser_name(), me.getFirst_name(), me.getLast_name(), tempPassword);
        return Response.ok("OK").build();
    }

    /**
     *
     * @param id
     * @param cookie
     * @param first_name
     * @param last_name
     * @param street_address
     * @param postal_code
     * @param city
     * @param email
     * @param phone
     * @param date_member
     * @param instrumentID
     * @param partID
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/alteruser/{USER_ID}")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> alterUser(@PathParam("USER_ID") int id,
            @CookieParam("access_token") Cookie cookie,
            @FormParam("first_name") String first_name,
            @FormParam("last_name") String last_name,
            @FormParam("street_address") String street_address,
            @FormParam("postal_code") int postal_code,
            @FormParam("city") String city,
            @FormParam("email") String email,
            @FormParam("phone") int phone,
            @FormParam("date_member") String date_member,
            @FormParam("instrument") int instrumentID,
            @FormParam("part") int partID) throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (!sr.isAdmin(cookie) && !sr.isArkivansvarlig(cookie)) {
            List<User> no = new ArrayList<>();
            return no;
        }
        User testme = getUser(id);
        User me = em.find(User.class, testme.getUser_id());
        me.setFirst_name(first_name);
        me.setLast_name(last_name);
        me.setStreet_address(street_address);
        me.setPostal_code(postal_code);
        me.setCity(city);
        me.setEmail(email);
        me.setPhone(phone);
        me.setDate_member(date_member);

        // create list of instruments connected to the user
        List<Instrument> userInstruments = new ArrayList<>();
        List result = em.createQuery("SELECT i FROM Instrument i WHERE (i.instrument_id) = (:paramID)", Instrument.class).setParameter("paramID", instrumentID).getResultList();
        Instrument i = null;
        me.setInstruments(userInstruments);
        if (!result.isEmpty()) {
            i = (Instrument) result.get(0);
            userInstruments.add(i);
            me.setInstruments(userInstruments);
        }

        // add parts connected to the users instrument
        List result1 = em.createQuery("SELECT p FROM Part p WHERE (p.part_id) = (:paramID)", Part.class).setParameter("paramID", partID).getResultList();
        if (!result1.isEmpty()) {
            me.setMyparts(result1);  
        } else if (result1.isEmpty()) {         
            List<Part> p = null;
            me.setMyparts(p);
        }

        if (date_member == null) {
            date_member = "";
        }
        me.updateDate_edited();
        em.persist(me);
        return em.createQuery(selectUsers, User.class).getResultList();
    }

    /**
     *
     * @param id
     * @param cookie
     * @param password1
     * @param password2
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    @Path("/alterpassword/{USER_ID}")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public void alterPassword(@PathParam("USER_ID") int id,
            @CookieParam("access_token") Cookie cookie,
            @FormParam("password1") String password1,
            @FormParam("password2") String password2) throws NoSuchAlgorithmException, InvalidKeySpecException {

        if (sr.isAdmin(cookie) || sr.isArkivansvarlig(cookie)) {

            User testme = getUser(id);
            User me = em.find(User.class, testme.getUser_id());

            if (!password1.isEmpty() || !password2.isEmpty()) {
                PasswordEncryptionService a = new PasswordEncryptionService();
                me.setSalt(a.generateSalt());
                me.setPassword(a.getEncryptedPassword(password1, me.getSalt()));
            }

            me.updateDate_edited();
            em.persist(me);
        }

    }

}

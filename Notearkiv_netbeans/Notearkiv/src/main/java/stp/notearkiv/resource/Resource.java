package stp.notearkiv.resource;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import io.jsonwebtoken.impl.crypto.MacProvider;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import stp.notearkiv.entity.Instrument;
import stp.notearkiv.entity.Musician;
import stp.notearkiv.entity.Part;
import stp.notearkiv.entity.Role;
import stp.notearkiv.entity.Score;
import stp.notearkiv.entity.SystemSetting;
import stp.notearkiv.entity.User;

/**
 * Class holding different types of resources and information used in various places around the application
 *
 * @author Team STP
 */
@Stateless
@Path("")

public class Resource {
    
    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;

    public static final Key KEY = MacProvider.generateKey();
    public final String version_number = getClass().getPackage().getImplementationVersion();

    public Resource() {
    }

    public static Key getKEY() {
        return KEY;
    }
    
    /**
     * Insert some data in database for testing purposes.
     *
     * @return Response message
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.spec.InvalidKeySpecException
     */
    @Path("/install")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String installTestData() throws NoSuchAlgorithmException, InvalidKeySpecException {
        
        
        // creating Role entities
        List<Role> check;
        check = em.createQuery("select r from Role r", Role.class).getResultList();
        if(check.isEmpty()){
        Role administrator = new Role("administrator");
        Role arkivansvarlig = new Role("arkivansvarlig");
        Role utoever = new Role("utøver");
        Role dirigent = new Role("dirigent");
        em.persist(administrator);
        em.persist(arkivansvarlig);
        em.persist(utoever);
        em.persist(dirigent);
        
        SystemSetting sys = new SystemSetting(
                "http://158.38.101.119:8080/",
                "http://158.38.101.119:8080/services/",
                version_number,
                "notearkivet.ntnu@gmail.com",
                "notearkivet123",
                "smtp.gmail.com",
                "465",
                "Arne Styve",
                "asty@ntnu.no",
                "notearkivet.ntnu@gmail.com",
                "<strong>Notearkivet er en komplett webapplikasjon for håndtering av digitale noter.</strong><br><br>\n" +
                "En stor, og voksende utfordring for ethvert korps, band eller storband, enten det er for barn, ungdom eller voksne, er håndtering av noter og notesett. <br>\n" +
                "Den vanligste måten å håndtere noter på i dag, er i form av utskrifter eller kopier i papirform. <br>\n" +
                "Disse notene deles gjerne ut til hver enkelt, og blir i noen tilfeller samlet inn igjen når de ikke lenger skal brukes. <br>\n" +
                "Dette er verken praktisk, effektivt eller spesielt miljøvennlig for den del. <br>\n" +
                "Stadig flere korps eller band begynner nå å håndtere sine notearkiver elektronisk.<br>\n" +
                "Dette kan gjøres mye mer effektivt og brukervennlig både for den som skal organisere og håndtere notearkivet, og spesielt for brukerne av det. <br><br>\n" +
                "<strong>Dette er bakgrunnen for at vi har utviklet dette systemet.</strong><br><br>\n" +
                "\n" +
                "<h4>Notearkivet er utviklet og planlagt av:</h4>\n" +
                "<strong>Produkteier:</strong><br>\n" +
                "Arne Styve<br><br>\n" +
                "\n" +
                "<strong>Utviklere:</strong><br>\n" +
                "Gaute Hjellbakk Pettersen<br>\n" +
                "Sindre Sjøholt<br>\n" +
                "Thomas Robert Tennøy<br>");
        em.persist(sys);
        
        
        // creating a few musicians/composers for testing
        Musician m3 = new Musician("Gary Moore");
        em.persist(m3);
         
        // creating a few scores for testing
        List<Musician> l3 = em.createQuery("SELECT m from Musician m WHERE m.name in ('Gary Moore')", Musician.class).getResultList();
        
        Score s0 = new Score(666, "Cold Day in Hell", "" ,Time.valueOf("00:03:18"), "10", "", "pdf/Cold Day in Hell/Cold Day in Hell - Full Score.pdf", "pdf/Cold Day in Hell/mp3/Gary Moore - Cold Day In Hell (HD).mp3", l3, l3, l3, l3);
        em.persist(s0);
          
        //creates empty list of instruments
        List<Instrument> iList = new ArrayList<>();
        
        // creating predefined Administrator user with role assigned
        List<Role> r1 = new ArrayList<>();
        r1.add(administrator);
        r1.add(arkivansvarlig);
        r1.add(utoever);
        User a = new User("Admin", "Administrator", "Admingaten 1", 1234, "ADMINBY", "admin@admin.no", 12345678, "10.03.2017", iList, "admin@admin.no", "passord", r1);
        em.persist(a);
        
        // creating predefined user for product owner Arne Styve
        List<Role> r3 = new ArrayList<>();
        r3.add(administrator);
        User arne = new User("Arne", "Styve", "Tørla", 6020, "ÅLESUND", "asty@ntnu.no", 90560299, "10.03.2017", iList, "asty@ntnu.no", "Satchmo71", r3);
        em.persist(arne);
        
        // return information about what has been added to the database
        return "Testdata lagt inn i databasen."; 
        } else{return "Du har allerede lagt inn testdata i databasen!";}
    }
}

package stp.notearkiv;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import stp.notearkiv.resource.SessionResource;
import stp.notearkiv.resource.SettingsResource;

/**
 * Class for sending email invitations. 
 * @author Team STP
 */

@Stateless
@Path("/email")
public class SendInvitation {
    
    @PersistenceContext(unitName = "Notearkivet")
    private EntityManager em;
    
    @Inject
    SettingsResource sr;
    
    @Inject
    SessionResource sessioncheck;
    
    String registrere = "registrere.html";
   
    /**
     *
     * @param email
     * @param subject
     * @param text
     * @param role_id
     * @return 
     * @throws java.io.UnsupportedEncodingException 
     */
    @Path("/invite")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response sendInvitations(@CookieParam("access_token") Cookie cookie,
                                    @FormParam("email") String email,
                                    @FormParam("subject") String subject,
                                    @FormParam("text") String text,
                                    @FormParam("id") String role_id ) throws UnsupportedEncodingException
        {
            if(!(sessioncheck.isAdmin(cookie) ) && !(sessioncheck.isArkivansvarlig(cookie))){
            return Response.serverError().entity("nope").build();
        }
            
           //Creating a list of the input from Front-end
           List<String> emailList = new ArrayList<>(Arrays.asList(email.split("\\s*,\\s*")));  
                //Email properties
		Properties props = new Properties();
		props.put("mail.smtp.host", sr.getEmailSmtp());
		props.put("mail.smtp.socketFactory.port", sr.getEmailPort());
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", sr.getEmailPort());
                
                //Creates and login session for Notearkivet Email account
		Session session = Session.getInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(sr.getEmailAddress(), sr.getEmailPassword());
				}
			});
                        
		try {
                        for(int i = 0; i< emailList.size(); i++ )
                        {
                            
                         //Generating an encoded registration link for each user. 
                         String encodedLink = sr.getServerUrl()
                         + java.net.URLEncoder.encode(registrere, "UTF-8") + "?"
                         + java.net.URLEncoder.encode("email", "UTF-8")
                         + "=" + java.net.URLEncoder.encode(emailList.get(i), "UTF-8") + "&id="
                         + java.net.URLEncoder.encode(role_id, "UTF-8");
                         
                         //Creating the email invitation
			 Message message = new MimeMessage(session);
			 message.setFrom(new InternetAddress(sr.getEmailAddress()));  
                         message.setRecipients(Message.RecipientType.TO,
                         InternetAddress.parse(emailList.get(i)));
			 message.setSubject(subject);
			 message.setText(text + System.lineSeparator()+ "Trykk på denne linken for å registrere deg som bruker: " + System.lineSeparator()+ encodedLink);
			 Transport.send(message);

                        } 
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} 
                return Response.status(200) 
                     .build();
	} 
}